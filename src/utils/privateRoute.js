import React from 'react';
import { Route, Redirect } from 'react-router-dom';

export const PrivateRoute = ({ component: Component, user, ...rest }) => {
    <Route
        {...rest}
        render={() =>
            user ? <Component {...rest} /> : <Redirect to='/login' />
        }
    />;
};
