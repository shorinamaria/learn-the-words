import {
    FETCH_CARD_LIST,
    FETCH_CARD_LIST_RESOLVE,
    FETCH_CARD_LIST_REJECT,
} from '../actions/actionTypes';

const cardListReducer = (
    state = { words: [], cardsPending: false },
    action
) => {
    switch (action.type) {
        case FETCH_CARD_LIST:
            return {
                ...state,
                words: [],
                cardsPending: true,
            };
        case FETCH_CARD_LIST_RESOLVE:
            return {
                ...state,
                words: action.payload,
                cardsPending: false,
            };
        case FETCH_CARD_LIST_REJECT:
            return {
                ...state,
                words: [],
                cardsPending: false,
            };
        default:
            return state;
    }
};

export default cardListReducer;
