import { combineReducers } from 'redux';
import userReducer from './userReducer';
import cardListReducer from './cardListReducer';
import authReducer from './authReducer';

export default combineReducers({
    userState: userReducer,
    cardListState: cardListReducer,
    authState: authReducer,
});
