import { ADD_USER } from '../actions/actionTypes';

const userReducer = (state = { user: null }, action) => {
    switch (action.type) {
        case ADD_USER:
            return {
                ...state,
                user: action.user,
                // todo ?
            };
        default:
            return state;
    }
};

export default userReducer;
