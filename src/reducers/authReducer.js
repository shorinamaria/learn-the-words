import {
    AUTH_SET_REGISTER_MODE,
    AUTH_SET_PENDING,
    AUTH_SET_ERROR,
} from '../actions/actionTypes';

const authReducer = (
    state = {
        error: null,
        pending: false,
        isRegister: false,
    },
    action
) => {
    switch (action.type) {
        case AUTH_SET_REGISTER_MODE:
            return {
                ...state,
                isRegister: action.payload,
                error: null,
                pending: false,
            };
        case AUTH_SET_PENDING:
            return {
                ...state,
                pending: action.payload,
                error: null,
            };
        case AUTH_SET_ERROR:
            return {
                ...state,
                error: action.error,
                pending: false,
            };
        default:
            return state;
    }
};

export default authReducer;
