import * as firebase from 'firebase/app';
import 'firebase/database';
import 'firebase/auth';
import CardsService from './cards';
import AuthService from './auth';

class Firebase {
    constructor() {
        firebase.initializeApp(
            JSON.parse(process.env.REACT_APP_FIREBASE_CONFIG)
        );
        this.auth = firebase.auth();
        this.database = firebase.database();

        this.userId = null;
        this.authService = new AuthService(this.auth);
    }

    setUser = (user) => {
        this.userId = user.uid;

        this.authService.user = user;
        this.cardsService = new CardsService(this.database, this.userId);
    };
}

export default Firebase;
