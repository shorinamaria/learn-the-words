export default class CardsService {
    constructor(database, userId) {
        this.database = database;
        this.url = `/cards/${userId}`;
    }

    getCards = () => {
        return this.database.ref(this.url);
    };

    getCardById = (id) => {
        return this.database.ref(`${this.url}/${id}`);
    };

    saveCard = async (cardDto) => {
        this.database.ref(`${this.url}/${cardDto.id}`).set(cardDto);
    };

    deleteCard = async (id) => {
        this.database.ref(`${this.url}/${id}`).remove();
    };
}
