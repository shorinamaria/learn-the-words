export default class AuthService {
    constructor(auth) {
        this.auth = auth;
        this.user = null;
    }

    signWithEmail = (email, password) =>
        this.auth.signInWithEmailAndPassword(email, password);

    registerWithEmail = (email, password) =>
        this.auth.createUserWithEmailAndPassword(email, password);

    signOut = async () => {
        this.user = null;
        return this.auth.signOut();
    };
}
