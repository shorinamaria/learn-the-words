import React from 'react';
import s from './Card.module.scss';
import cl from 'classnames';
import {
    CheckSquareOutlined,
    BorderOutlined,
    DeleteOutlined,
} from '@ant-design/icons';
import FirebaseContext from '../../context/firebaseContext';
import { Link, withRouter } from 'react-router-dom';

class Card extends React.Component {
    state = {
        done: false,
        remembered: false,
    };

    cardsService = this.context.cardsService;

    handleCardClick = () => {
        this.setState((state) => ({ done: !state.done }));
    };

    handleRememberedClick = () => {
        this.setState((state) => ({
            remembered: !state.remembered,
            done: true,
        }));
    };

    handleDeleteClick = () => {
        this.cardsService.deleteCard(this.props.id);
    };

    componentDidMount() {
        const {
            id: currentId,
            match: {
                params: { id, done },
            },
        } = this.props;

        if (+id === currentId) {
            this.setState({ done: Boolean(done) });
        }
    }

    render() {
        const { id, eng, rus } = this.props;
        const { done, remembered } = this.state;

        return (
            <article
                className={cl(
                    s.card,
                    { [s.done]: done },
                    { [s.remembered]: remembered }
                )}
            >
                <button
                    className={cl('button-reset', s.cardInner)}
                    type='button'
                    disabled={this.state.remembered}
                    onClick={this.handleCardClick}
                >
                    <p className={s.cardFront}>{eng}</p>
                    <p className={s.cardBack}>{rus}</p>
                </button>
                <button
                    className={cl(
                        'button-reset',
                        'button-icon',
                        s.cardRememberButton
                    )}
                    type='button'
                    disabled={this.state.remembered}
                    onClick={this.handleRememberedClick}
                >
                    {this.state.remembered ? (
                        <CheckSquareOutlined />
                    ) : (
                        <BorderOutlined />
                    )}
                </button>
                <button
                    className={cl(
                        'button-reset',
                        'button-icon',
                        s.cardDeleteButton
                    )}
                    type='button'
                    title='Удаляем карточку'
                    onClick={this.handleDeleteClick}
                >
                    <DeleteOutlined />
                </button>
                <Link to={`/card/${id}`}>Edit</Link>
            </article>
        );
    }
}

Card.contextType = FirebaseContext;

export default withRouter(Card);
