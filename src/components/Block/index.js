import React from 'react';
import s from './Block.module.scss';
import cl from 'classnames';

const Block = ({
    backgroundImage,
    backgroundColor,
    backgroundGradient,
    children,
    className,
}) => {
    const styleBlock = {};
    if (backgroundImage) {
        Object.assign(styleBlock, {
            backgroundImage: `url(${backgroundImage}`,
        });
    }
    if (backgroundColor) {
        Object.assign(styleBlock, { backgroundColor });
    }
    if (backgroundGradient) {
        Object.assign(styleBlock);
    }
    return (
        <div
            className={cl(
                s.block,
                { [s.blockGradient]: backgroundGradient },
                className
            )}
            style={styleBlock}
        >
            <div className={s.blockContentWrapper}>{children}</div>
        </div>
    );
};

export default Block;
