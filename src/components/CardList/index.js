import React, { Component } from 'react';
import Card from '../Card';
import Block from '../Block';
import s from './CardList.module.scss';
import cl from 'classnames';
import { RightOutlined, SyncOutlined } from '@ant-design/icons';
import { Input, Button, AutoComplete, Spin } from 'antd';
import getTranslatedWord from '../../services/yandex.dictionary';
import { debounce } from '../../utils';
import FirebaseContext from '../../context/firebaseContext';

class CardList extends Component {
    state = {
        eng: '',
        rus: '',
        pending: false,
        words: [],
    };

    cardsService = this.context.cardsService;
    wordFromInput = React.createRef();

    handleInputChangeFrom = (e) => {
        this.setState({ eng: e.target.value });
        this.handleTranslateDebounced();
    };

    handleTranslateDebounced = debounce(() => {
        this.setState({ pending: true }, this.getTranslation);
    }, 300);

    handleInputChangeTo = (value) => {
        this.setState({ rus: value });
    };

    getTranslation = async () => {
        const word = await getTranslatedWord(this.state.eng);
        const words = word.flatMap((el) => el.tr.map((tr) => tr.text));

        this.setState({
            pending: false,
            words,
            rus: words[0] || '',
        });
    };

    handleAddCardClick = () => {
        const { eng, rus } = this.state;
        const cardDto = { id: Date.now(), eng, rus };
        this.cardsService.saveCard(cardDto).then(() => {
            this.setState({
                rus: '',
                eng: '',
                words: [],
            });
            this.wordFromInput.current.focus();
        });
    };

    render() {
        const { items = [], cardsPending } = this.props;
        const { pending, words, rus, eng } = this.state;

        const options =
            words.length <= 1
                ? []
                : words.map((word) => ({
                      value: word,
                  }));

        return (
            <React.Fragment>
                <Block backgroundColor='#f7f7f7'>
                    <section className={cl(s.wordsInputsWrapper)}>
                        <h2 className='visually-hidden'>
                            Здесь можно добавить новое слово для изучения
                        </h2>
                        <label className={cl(s.wordsField)}>
                            <p className={cl(s.wordsFieldTitle)}>
                                Введи слово для изучения
                            </p>
                            <div className={cl(s.wordsInputBlock)}>
                                <Input
                                    className={cl(s.wordsInput)}
                                    placeholder='read'
                                    size='large'
                                    value={eng}
                                    ref={this.wordFromInput}
                                    onChange={this.handleInputChangeFrom}
                                />
                                <span
                                    className={cl(
                                        'button-icon',
                                        s.wordsInputsSeparator
                                    )}
                                >
                                    {pending ? (
                                        <SyncOutlined spin />
                                    ) : (
                                        <RightOutlined />
                                    )}
                                </span>
                            </div>
                        </label>

                        <label className={cl(s.wordsField)}>
                            <p className={cl(s.wordsFieldTitle)}>Это перевод</p>
                            <AutoComplete
                                className={cl(s.wordsInput)}
                                placeholder='читать'
                                value={rus}
                                size='large'
                                options={options}
                                onChange={this.handleInputChangeTo}
                            />
                            <p className={cl(s.wordsInputDescription)}>
                                Можно выбрать другой вариант из списка, или
                                ввести свой
                            </p>
                        </label>
                        <Button
                            type='primary'
                            size='large'
                            disabled={!rus || !eng}
                            style={{ marginTop: '41px' }} // todo: remove
                            onClick={this.handleAddCardClick}
                            onKeyPress={(event) => {
                                if (event.key === 'Enter') {
                                    this.handleAddCardClick();
                                }
                            }}
                        >
                            Добавить
                        </Button>
                    </section>
                </Block>
                <Block backgroundColor='#88b593'>
                    {/*  #66a075*/}
                    <div className={cl(s.wordsBlock)}>
                        {cardsPending ? (
                            <Spin size='large' />
                        ) : (
                            <React.Fragment>
                                {items.length ? (
                                    <ul className={s.wordsList}>
                                        {items.map(({ eng, rus, id }) => (
                                            <li
                                                key={id}
                                                className={s.wordsListItem}
                                            >
                                                <Card
                                                    id={id}
                                                    rus={rus}
                                                    eng={eng}
                                                />
                                            </li>
                                        ))}
                                    </ul>
                                ) : (
                                    <p className={s.wordsEmpty}>
                                        У тебя пока нет слов для изучения,
                                        <br />
                                        добавь их :)
                                    </p>
                                )}
                            </React.Fragment>
                        )}
                    </div>
                </Block>
            </React.Fragment>
        );
    }
}

CardList.contextType = FirebaseContext;

export default CardList;
