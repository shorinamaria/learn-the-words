import React from 'react';
import s from './HeaderBlock.module.scss';
import Block from '../Block';
import HeaderBackground from './../../images/background.jpg';
import Header from '../Header';
import Paragraph from '../Paragraph';
import { ReactComponent as ReactLogoSvg } from '../../images/logo.svg';

const HeaderBlock = (props) => {
    return (
        <header className={s.headerBlock}>
            <Block backgroundImage={HeaderBackground} backgroundGradient>
                <Header>Время учить слова онлайн</Header>
                <ReactLogoSvg />
                <Paragraph>
                    Используйте карточки для запоминания
                    <br />и пополняйте активный словарный запас
                </Paragraph>
            </Block>
        </header>
    );
};

export default HeaderBlock;
