import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { Route, Switch } from 'react-router-dom';
import { connect } from 'react-redux';

import LoginPage from '../../pages/Login';
import HomePage from '../../pages/Home';
import FirebaseContext from '../../context/firebaseContext';
import TopPanel from '../TopPanel';
import CardPage from '../../pages/CardPage';
import { bindActionCreators } from 'redux';
import { addUserAction } from '../../actions/userAction';

class App extends Component {
    componentDidMount() {
        const { auth, setUser } = this.context;
        const { addUser } = this.props;
        auth.onAuthStateChanged((user) => {
            if (user) {
                setUser(user);
            }
            addUser(user || false);
        });
    }

    render() {
        const { user } = this.props;

        if (user === null) {
            return null;
        }

        return (
            <Switch>
                <Route
                    path='/login'
                    render={() =>
                        user ? <Redirect to='/home' /> : <LoginPage />
                    }
                />
                <Route
                    render={() =>
                        !user ? (
                            <LoginPage />
                        ) : (
                            <React.Fragment>
                                <TopPanel />
                                <Switch>
                                    <Route
                                        path='/'
                                        exact
                                        component={HomePage}
                                    />
                                    <Route
                                        path='/home/:id?/:done?'
                                        component={HomePage}
                                    />
                                    <Route
                                        path='/about'
                                        render={() => <h1>About page</h1>}
                                    />
                                    <Route
                                        path='/card/:id?'
                                        render={CardPage}
                                    />
                                    <Route
                                        render={() => <Redirect to='/home' />}
                                    />
                                </Switch>
                            </React.Fragment>
                        )
                    }
                />
            </Switch>
        );
    }
}

App.contextType = FirebaseContext;

const mapStateToProps = (state) => {
    return {
        user: state.userState.user,
    };
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators(
        {
            addUser: addUserAction,
        },
        dispatch
    );
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
