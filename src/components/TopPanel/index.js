import React, { useContext } from 'react';
import s from './TopPanel.module.scss';
import cl from 'classnames';
import FirebaseContext from '../../context/firebaseContext';
import { Link, useHistory } from 'react-router-dom';

const TopPanel = () => {
    const history = useHistory();
    const { authService } = useContext(FirebaseContext);
    const { user, signOut } = authService;

    const handleLogout = () => {
        signOut().then(() => {
            history.push('/home');
        });
    };

    return (
        <div className={s.topPanel}>
            <nav>
                <ul className={cl(s.topPanelMenuList)}>
                    <li className={cl(s.topPanelMenuItem, s.topPanelLogo)}>
                        <Link to='/home'>Learn the words</Link>
                    </li>
                    <li className={cl(s.topPanelMenuItem)}>
                        <Link to='/about'>About</Link>
                    </li>
                </ul>
            </nav>
            <div>
                <React.Fragment>
                    <b style={{ marginRight: '20px' }}>{user.email}</b>
                    <button
                        className={cl('button-reset')}
                        type='button'
                        onClick={handleLogout}
                    >
                        logout
                    </button>
                </React.Fragment>
            </div>
        </div>
    );
};

export default TopPanel;
