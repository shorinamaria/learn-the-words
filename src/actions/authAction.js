import {
    AUTH_SET_ERROR,
    AUTH_SET_PENDING,
    AUTH_SET_REGISTER_MODE,
} from './actionTypes';

export const auth = (getData, onSuccess) => {
    return (dispatch, getState) => {
        dispatch(authSetPendingAction(true));
        getData()
            .finally(() => {
                dispatch(authSetPendingAction(false));
            })
            .then(() => onSuccess && onSuccess())
            .catch((error) => {
                // console.log('============error', error);
                dispatch(authSetErrorAction(`Ошибка авторизации`));
            });
    };
};

export const authSetErrorAction = (error) => ({
    type: AUTH_SET_ERROR,
    error,
});

export const authSetPendingAction = (payload) => ({
    type: AUTH_SET_PENDING,
    payload,
});

export const authSetRegisterModeAction = (payload) => ({
    type: AUTH_SET_REGISTER_MODE,
    payload,
});
