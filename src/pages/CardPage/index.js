import React, { PureComponent } from 'react';
import { withFirebase } from '../../context/firebaseContext';
import Card from '../../components/Card';
import Block from '../../components/Block';
import s from './CardPage.module.scss';
import { Spin } from 'antd';

class CardPage extends PureComponent {
    state = {
        card: null,
        pending: true,
    };

    componentDidMount() {
        const {
            firebase: { cardsService },
            match: {
                params: { id },
            },
        } = this.props;

        cardsService
            .getCardById(id)
            .once('value')
            .then((res) => {
                const card = res.val();
                if (card) {
                    this.setState({
                        card,
                    });
                }
            })
            .finally(() => this.setState({ pending: false }));
    }

    render() {
        const { card, pending } = this.state;

        return (
            <Block backgroundColor='#f7f7f7' className={s.cardsPage}>
                {pending ? (
                    <Spin />
                ) : !card ? (
                    <div className='CardPageWrapper'>Слово не найдено</div>
                ) : (
                    <Card {...card} />
                )}
            </Block>
        );
    }
}

export default withFirebase(CardPage);
