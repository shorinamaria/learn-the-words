import React, { PureComponent } from 'react';
import { Layout, Form, Input, Button, Spin } from 'antd';

import s from './Login.module.scss';
import cl from 'classnames';
import { withFirebase } from '../../context/firebaseContext';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
    auth,
    authSetRegisterModeAction,
    authSetPendingAction,
} from '../../actions/authAction';

const { Content } = Layout;

class LoginPage extends PureComponent {
    authService = this.props.firebase.authService;

    onFinish = ({ email, password }) => {
        const { history, auth } = this.props;

        auth(
            () => this.authService.signWithEmail(email, password),
            () => history && history.push('/')
        );
    };

    onFinishFailed = () => {
        const { setPending } = this.props;
        setPending(false);
    };

    onFinishRegister = ({ email, password }) => {
        const { auth } = this.props;
        auth(() => this.authService.registerWithEmail(email, password));
    };

    renderLoginForm() {
        const { setRegisterMode } = this.props;
        const layout = {
            labelCol: { span: 6 },
            wrapperCol: { span: 18 },
        };
        const tailLayout = {
            wrapperCol: { offset: 6, span: 18 },
        };
        return (
            <Form
                {...layout}
                name='loginForm'
                onFinish={this.onFinish}
                onFinishFailed={this.onFinishFailed}
            >
                <Form.Item
                    label='Email'
                    name='email'
                    rules={[
                        {
                            type: 'email',
                            message: `Email какой-то странный`,
                        },
                        {
                            required: true,
                            message: `Ты забыл ввести Email`,
                        },
                    ]}
                >
                    <Input />
                </Form.Item>

                <Form.Item
                    label='Пароль'
                    name='password'
                    rules={[
                        {
                            required: true,
                            message: `Ты забыл ввести пароль`,
                        },
                    ]}
                >
                    <Input.Password />
                </Form.Item>

                <Form.Item {...tailLayout}>
                    <Button type='primary' htmlType='submit'>
                        Войти
                    </Button>
                    &nbsp;&nbsp;&nbsp;&nbsp;или&nbsp;
                    <button
                        className={cl('button-reset', 'link')}
                        type='button'
                        onClick={() => setRegisterMode(true)}
                    >
                        Зарегистрироваться
                    </button>
                </Form.Item>
            </Form>
        );
    }

    renderRegisterForm() {
        const { setRegisterMode } = this.props;
        const layout = {
            labelCol: { span: 6 },
            wrapperCol: { span: 18 },
        };
        const tailLayout = {
            wrapperCol: { offset: 6, span: 18 },
        };
        return (
            <Form
                {...layout}
                name='registerForm'
                onFinish={this.onFinishRegister}
                onFinishFailed={this.onFinishFailed}
            >
                <Form.Item
                    label='Email'
                    name='email'
                    rules={[
                        {
                            type: 'email',
                            message: `Email какой-то странный`,
                        },
                        {
                            required: true,
                            message: `Ты забыл ввести Email`,
                        },
                    ]}
                >
                    <Input />
                </Form.Item>

                <Form.Item
                    label='Пароль'
                    name='password'
                    rules={[
                        {
                            required: true,
                            message: `Ты забыл ввести пароль`,
                        },
                        {
                            len: 6,
                            message: `Должно быть ровно 6 символов`,
                        },
                    ]}
                >
                    <Input.Password />
                </Form.Item>

                <Form.Item {...tailLayout}>
                    <Button type='primary' htmlType='submit'>
                        Зарегистрироваться
                    </Button>
                    &nbsp;&nbsp;&nbsp;&nbsp;или&nbsp;
                    <button
                        className={cl('button-reset', 'link')}
                        type='button'
                        onClick={() => setRegisterMode(false)}
                    >
                        Войти
                    </button>
                </Form.Item>
            </Form>
        );
    }

    render() {
        const { error, pending, isRegister } = this.props;

        return (
            <Layout>
                <Content className={s.LoginMain}>
                    <section className={s.LoginFormWrapper}>
                        {isRegister
                            ? this.renderRegisterForm()
                            : this.renderLoginForm()}
                        <div className={cl(s.LoginError, 'error-text')}>
                            {pending ? (
                                <Spin tip='Пробуем авторизоваться...' />
                            ) : (
                                error || null
                            )}
                        </div>
                    </section>
                </Content>
            </Layout>
        );
    }
}

const mapStateToProps = (state) => ({
    error: state.authState.error,
    pending: state.authState.pending,
    isRegister: state.authState.isRegister,
});

const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {
            auth,
            setRegisterMode: authSetRegisterModeAction,
            setPending: authSetPendingAction,
        },
        dispatch
    );

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(withFirebase(LoginPage));
