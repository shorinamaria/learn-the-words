import React, { Component } from 'react';
import HeaderBlock from '../../components/HeaderBlock';
import CardList from '../../components/CardList';
import Footer from '../../components/Footer';

import FirebaseContext from '../../context/firebaseContext';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchCardList } from '../../actions/cardListAction';

class HomePage extends Component {
    cardsService = this.context.cardsService;

    componentDidMount() {
        if (!this.context.userId) {
            return;
        }

        const { fetchCardList } = this.props;

        fetchCardList(this.cardsService.getCards);
    }

    componentWillUnmount() {
        this.cardsService.getCards().off();
    }

    render() {
        const { words, cardsPending } = this.props;
        return (
            <React.Fragment>
                <HeaderBlock />
                <main>
                    <header className='visually-hidden'>
                        Список слов для изучения
                    </header>
                    <CardList items={words} cardsPending={cardsPending} />
                </main>
                <Footer />
            </React.Fragment>
        );
    }
}

HomePage.contextType = FirebaseContext;

const mapStateToProps = (state) => {
    return {
        words: state.cardListState.words,
        cardsPending: state.cardListState.cardsPending,
    };
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators(
        {
            fetchCardList,
        },
        dispatch
    );
};

export default connect(mapStateToProps, mapDispatchToProps)(HomePage);
