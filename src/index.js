import './index.scss';
import 'antd/dist/antd.css';

import React from 'react';
import ReactDom from 'react-dom';
import App from './components/App';
import FirebaseContext from './context/firebaseContext';
import Firebase from './services/firebase';
import { BrowserRouter } from 'react-router-dom';

import { applyMiddleware, createStore } from 'redux';
import rootReducer from './reducers';
import { Provider } from 'react-redux';
import { logger } from 'redux-logger/src';
import thunk from 'redux-thunk';

const store = new createStore(rootReducer, applyMiddleware(thunk, logger));

ReactDom.render(
    <Provider store={store}>
        <FirebaseContext.Provider value={new Firebase()}>
            <BrowserRouter>
                <App />
            </BrowserRouter>
        </FirebaseContext.Provider>
    </Provider>,
    document.getElementById('root')
);
